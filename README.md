# INVERSE MATRIX PJC by DAVID HAJEK

Repozitář určený pro vývoj semestrální práce z předmětu PJC na téma Výpočet inverzní matice.

# Popis

Aplikace počítá inverzní matice N*N. Musíte zadat matici ve správné velikosti, jinak se výpočet neprovede.
Pro výpočet inverzních matic můžete využít buď připravené matice - pod příkazem '--test velikost', nebo můžete vložit
vlastní matici přes příkazový řádek - pod příkazem '--start'. Více vláknost můžete nastavit pmocí příkazů '--single_thread'
a '--multi_thread'. Defaultně je nastaveno více vláken, které se řídí počtem jader v počítači. K výpočtu používám gausovu eliminaci.

# Návod

Application arguments:
  Type --help to get more info
  Type --start to calculate inverse matrix
  Type --single_thread to use single thread
  Type --multi_thread to use single thread
  Type --end to end program
Warning:
  - Your matrix must be N*N
Tests:
  Type '--test n' where n can be {3, 4, 5, 15, 700} to test default matrix

# Časy
Matice 700 x 700 v jednom vláknu potřebovala pro výpočet 88 594 411 mikro sekund.
Matice 700 x 700 ve více vláknech (4 na mém počítači) potřebovala pro výpočet 61 393 761 mikro sekund.

Při menších maticí nedochazí ke zrychlení kvuli naročnosti vytváření jader.
Pro větší matice nevypisuji výsledek, jelikož to trvá moc dlouho. Dá se to lehce změnit v kodu v souboru cmd.cpp v calculationPhase calculation.