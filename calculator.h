#include <vector>

using namespace std;

class calculator {
private:
    vector<vector<double>> A;
public:
    void calculator::doJob(int k, int i, int j, int n);
    vector<vector<double>> calculateInverse(vector<vector<double>>& A, bool multiThreading);

};
