#include <iostream>
#include "cmd.h"
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    cmd cmdl = cmd();

    string result;
    while(getline(cin, result)){
        if (cin.bad())
            throw runtime_error("Bad read");
        if (cin.eof() || result=="end")
            break;
        if (cin.good()) {
            try {
                cmdl.parse(result);
            } catch (runtime_error &e) {
                cout << e.what() << endl;
                break;
            }
        }
    }

}