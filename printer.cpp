#include "printer.h"
#include <string>



void printer::print(const string &content) const {
    cout << content << endl;
}

void printer::intro() const {
    print("\t INVERTING MATRIX");
    print("David Hajek | type --help for more info");
}

void printer::help() const {
    print("\tHELP");
    print("Application arguments:");
    print("  Type --help to get more info");
    print("  Type --start to calculate inverse matrix");
    print("  Type --single_thread to use single thread");
    print("  Type --multi_thread to use single thread");
    print("  Type --end to end program");
    print("Warning:");
    print("  - Your matrix must be N*N");
    print("Tests:");
    print("  Type '--test n' where n can be {3, 4, 5, 15, 700} to test default matrix");
    print("\n");
}

void printer::calculation() const {
    print("First step: enter size of matrix");
    print("Second step: fill matrix");
    print("Third step: calculate inverse matrix");
    print("\n");

}

void printer::time(long time, bool multithreading) const {
    if (multithreading){
        print("Time needed to complete calculation with multiple threads was " + to_string(time) + "μs.");
    } else {
        print("Time needed to complete calculation with single thread was " + to_string(time) + "μs.");
    }
}

void printer::printNumberOfLine(const int &line) const {
    cout << "Line number "<< line + 1 << endl;


}

void printer::printMatrix(vector< vector<double> > A) const{
    int n = A.size();
    for (int i=0; i<n; i++) {
        for (int j=0; j<n; j++) {
            cout << A[i][j] << "\t";
        }
        cout << endl;
    }
    cout << endl;
}


void printer::printGaussianMatrix(vector< vector<double> > A) const{
    int n = A.size();
    for (int i=0; i<n; i++) {
        for (int j=0; j<2*n; j++) {
            cout << A[i][j] << "\t";
            if (j == n-1) {
                cout << "| ";
            }
        }
        cout << "\n";
    }
    cout << endl;
}