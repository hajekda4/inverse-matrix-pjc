#include "cmd.h"
#include <string>
#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

void cmd::parse(string &command) {

    if (command == "--help") {
        help();
        return;
    }

    if (command == "--multi_thread") {
        multiThreading = true;
        p.print("Application is running on multiple threads");
        return;
    }

    if (command == "--single_thread") {
        multiThreading = false;
        p.print("Application is running on single thread");
        return;
    }

    if (command == "--start") {
        p.calculation();
        calculationPhase = "size";
        p.print("Enter size");
        return;
    }

    if (calculationPhase == "size") {
        if (isNumber(command)) {
            size = stoi(command);
        } else {
            throw runtime_error("invalid command");
        }
        A = vector<vector<double>>(static_cast<size_t>(size));

        line = 0;
        calculationPhase = "filling";
        p.print("size of the matrix is " + std::to_string(size));
        p.print("Now fill the matrix by lines");
        p.printNumberOfLine(line++);

        return;
    }

    if (calculationPhase == "filling") {

        vector<string> charVec = split(command);
        vector<double> doubleVec = vector<double>(charVec.size());
        for (int i=0; i < static_cast<int>(charVec.size()); i++) {
            doubleVec[i] = stod(charVec[i]); //warning
        }
        if (charVec.size() != size) {
            throw runtime_error("Invalid number of numbers");
        }

        A[line - 1] = doubleVec;

        if (line + 1 == size) {
            p.print("Last line");
            line++;
        } else if (line == size) {
            calculationPhase = "calculation";
            p.print("Your matrix");
            p.printMatrix(A);
            p.print("Press Enter to calculate inverse matrix");
        } else {
            p.printNumberOfLine(line++);
        }

        return;
    }

    if (calculationPhase == "calculation") {

        p.print("Calculating inverse matrix.....");

        vector<double> line(2 * size, 0);
        vector<vector<double>> B(size, line);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                B[i][j] = A[i][j];
            }
        }

        for (int i = 0; i < size; i++) {
            B[i][size + i] = 1;
        }

        if (size > 50) {
            cout << "start " << endl;
        } else {
            p.printGaussianMatrix(B);
        }
        // start time, pass whether to use multi threads
        auto start = chrono::high_resolution_clock::now();
        vector<vector<double>> resultA = c.calculateInverse(B, multiThreading);
        auto end = chrono::high_resolution_clock::now();
        //stop time

        if (size > 50) {
            cout << "end" << endl;
        } else {
            cout << "end" << endl;
            p.printGaussianMatrix(resultA);
        }
        p.time((unsigned long)chrono::duration_cast<chrono::microseconds>(end - start).count(), multiThreading);

        calculationPhase = "waiting";
    }


    if (command == "--test 3") {

        size = 3;
        A = vector<vector<double>>(3);
        A[0] = vector<double>{3, -4, 5};
        A[1] = vector<double>{2, -3, 1};
        A[2] = vector<double>{3, -5, -1};


        calculationPhase = "calculation";
        p.print("Your matrix");
        p.printMatrix(A);
        p.print("Press Enter to calculate inverse matrix");
        return;
    }

    if (command == "--test 4") {

        size = 4;
        A = vector<vector<double>>(4);
        A[0] = vector<double>{3, 2, 1, 2};
        A[1] = vector<double>{7, 5, 2, 5};
        A[2] = vector<double>{0, 0, 9, 4};
        A[3] = vector<double>{0, 0, 11, 5};


        calculationPhase = "calculation";
        p.print("Your matrix");
        p.printMatrix(A);
        p.print("Press Enter to calculate inverse matrix");
        return;
    }

    if (command == "--test 5") {

        size = 5;
        A = vector<vector<double>>(5);
        A[0] = vector<double>{4, 6, 5, 7, 11};
        A[1] = vector<double>{13, 14, 8, 9, 10};
        A[2] = vector<double>{12, 15, 16, 17, 18};
        A[3] = vector<double>{19, 3, 2, 20, 21};
        A[4] = vector<double>{22, 23, 24, 25, 26};


        calculationPhase = "calculation";
        p.print("Your matrix");
        p.printMatrix(A);
        p.print("Press Enter to calculate inverse matrix");
        return;
    }

    if (command == "--test 15") {

        size = 15;
        A = vector<vector<double>>(15);
        A[0] = vector<double>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        A[1] = vector<double>{10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        A[2] = vector<double>{10, 20, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        A[3] = vector<double>{10, 2, 30, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        A[4] = vector<double>{10, 2, 3, 40, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        A[5] = vector<double>{10, 2, 3, 4, 50, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        A[6] = vector<double>{10, 2, 3, 4, 5, 60, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        A[7] = vector<double>{10, 2, 3, 4, 5, 6, 70, 8, 9, 10, 11, 12, 13, 14, 15};
        A[8] = vector<double>{10, 2, 3, 4, 5, 6, 7, 80, 9, 10, 11, 12, 13, 14, 15};
        A[9] = vector<double>{10, 2, 3, 4, 5, 6, 7, 8, 90, 10, 11, 12, 13, 14, 15};
        A[10] = vector<double>{10, 2, 3, 4, 5, 6, 7, 8, 9, 100, 11, 12, 13, 14, 15};
        A[11] = vector<double>{10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 110, 12, 13, 14, 15};
        A[12] = vector<double>{10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 120, 13, 14, 15};
        A[13] = vector<double>{10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 130, 14, 15};
        A[14] = vector<double>{10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 140, 15};


        calculationPhase = "calculation";
        p.print("Your matrix");
        p.printMatrix(A);
        p.print("Press Enter to calculate inverse matrix");
        return;
    }

    if (command == "--test 700") {
        size = 700;
        A = vector<vector<double>>(700);
        for (int i = 0; i < 700; ++i) {
            vector<double> row = vector<double>(700);
            for (int j = 0; j < 700; ++j) {
                row[j] = i*10 + j;
            }
            A[i] = row;
        }
        calculationPhase = "calculation";
        p.print("Your matrix is big - no print");
        p.print("Press Enter to calculate inverse matrix");
        return;
    }
}

vector<string> split(string const &line) {
    vector<string> res;
    if (line.empty())
        return res;
    size_t first = 0;
    size_t last;
    while ((last = line.find(" ", first)) != string::npos) {
        res.push_back(line.substr(first, last-first));
        first = last+1;
    }
    res.push_back(line.substr(first));
    return res;
}

cmd::cmd() {
    p.intro();
}

void cmd::help() const {
    p.help();
}

bool cmd::isNumber(const std::string &s) {
    std::string::const_iterator it = s.begin();
    while (it != s.end() && isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

