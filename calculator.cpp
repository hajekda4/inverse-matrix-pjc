#include "calculator.h"
#include <string>
#include <thread>
#include <iostream>
#include <list>
#include <future>

void calculator::doJob(int i, int from, int to, int n){
    if (to + 1 <= n) {
        to = to + 1;
    }
    for (int k = from; k < to; k++) {
        double c = -A[k][i] / A[i][i];

        for (int j = i; j < 2 * n; j++) {
            if (i == j) {
                A[k][j] = 0;
            } else {
                A[k][j] += c * A[i][j];
            }
        }
    }
}

vector<vector<double>> calculator::calculateInverse(vector<vector<double>>& B, bool multiThreading) {
    int n = B.size();
    A = B;
    for (int i = 0; i < n; i++) {

        // Search for maximum in this column
        double maxEl = abs(A[i][i]);
        int maxRow = i;
        for (int k = i + 1; k < n; k++) {
            if (abs(A[k][i]) > maxEl) {
                maxEl = A[k][i];
                maxRow = k;
            }
        }

        // Swap maximum row with current row (column by column)
        for (int k = i; k < 2 * n; k++) {
            double tmp = A[maxRow][k];
            A[maxRow][k] = A[i][k];
            A[i][k] = tmp;
        }

        //threading start
        int hardwareConcurrency = thread::hardware_concurrency();

        if (!multiThreading) {
            hardwareConcurrency = 1;
        }
//        int hardwareConcurrency = 4;
        vector<thread> threads;
        threads.reserve(hardwareConcurrency);

        int curr = i + 1; //
        for (int q = 0; q < hardwareConcurrency; q++) {
            int from, to;
            if (hardwareConcurrency == 1) {
                from = curr;
                to = n - 1;
            } else {
                if ((n - i - 1) < hardwareConcurrency) {
                    from = curr;
                    to = n - 1;
                } else {
                    if (curr + (n - i - 1)/hardwareConcurrency <= n - 1) {
                        from = curr;
                        to = curr + (n - i - 1)/hardwareConcurrency ;
                    } else {
                        from = curr;
                        to = n - 1;
                    }
                }
            }

            threads.emplace_back(&calculator::doJob, this, i, from, to, n);
            if (to == n - 1) {
                break;
            }
            curr = to + 1;
        }


        for (auto &thread : threads) {
            thread.join();
        }

        ///end of threading
    }

    // Solve equation Ax=b for an upper triangular matrix A
    for (int i = n - 1; i >= 0; i--) {
        for (int k = n; k < 2 * n; k++) {
            A[i][k] /= A[i][i];
        }
        A[i][i] = 1;

        for (int rowModify = i - 1; rowModify >= 0; rowModify--) {
            for (int columModify = n; columModify < 2 * n; columModify++) {
                A[rowModify][columModify] -= A[i][columModify] * A[rowModify][i];
            }
            A[rowModify][i] = 0;
        }
    }


    return A;
}
