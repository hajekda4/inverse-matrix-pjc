
#include <vector>
#include "calculator.h"
#include "printer.h"
#include <chrono>
#include <thread>

using namespace std;

class cmd {
    string calculationPhase;
    printer p = printer();
    calculator c = calculator();

    int line;
    int size;
    vector<vector<double>> A;
    bool multiThreading = true;

    bool isNumber(const std::string& s);

public:
    cmd();
    void parse(string &command);
    void help() const;
};

vector<string> split(string const &line);

