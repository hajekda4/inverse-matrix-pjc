#include <iostream>
#include <chrono>
#include <thread>
#include <vector>

using namespace std;

class printer {
public:
    void print(const string &content) const;
    void printNumberOfLine(const int &line) const;
    void printMatrix(vector<vector<double>> A) const;
    void printGaussianMatrix(vector<vector<double>> A) const;
    void time(long time, bool multithreading) const;

    void intro() const;
    void help() const;
    void calculation() const;
};

